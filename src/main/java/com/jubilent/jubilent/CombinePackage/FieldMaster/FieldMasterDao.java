package com.jubilent.jubilent.CombinePackage.FieldMaster;

import javax.persistence.*;

@Entity
@Table(name = "Tbl_FieldMaster")
public class FieldMasterDao {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long fieldId;
    private String fieldName;
    private boolean pivot;
    private boolean status;

    public long getFieldId() {
        return fieldId;
    }

    public void setFieldId(long fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public boolean isPivot() {
        return pivot;
    }

    public void setPivot(boolean pivot) {
        this.pivot = pivot;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FieldMasterDao{" +
                "id=" + fieldId +
                ", fieldname='" + fieldName + '\'' +
                ", pivot=" + pivot +
                ", status=" + status +
                '}';
    }
}
