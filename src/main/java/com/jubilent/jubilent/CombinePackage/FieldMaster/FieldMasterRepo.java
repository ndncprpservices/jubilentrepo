package com.jubilent.jubilent.CombinePackage.FieldMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FieldMasterRepo extends JpaRepository<FieldMasterDao,Long> {
    FieldMasterDao findByFieldId(long fieldId);
}
