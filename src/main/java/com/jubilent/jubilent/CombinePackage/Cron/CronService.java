package com.jubilent.jubilent.CombinePackage.Cron;

import java.util.List;

public interface CronService {
    CronDao saveCron (CronDao cronDao);
    CronDao getByDate(String date);
    List<CronDao> getAll();

}
