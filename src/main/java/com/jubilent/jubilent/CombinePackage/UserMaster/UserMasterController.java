package com.jubilent.jubilent.CombinePackage.UserMaster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/UserMaster")
public class UserMasterController {


    @Autowired
    UserMasterService userMasterService;

    @PostMapping ("/insertUser")
    public UserMasterDao insertUser(@RequestBody UserMasterDao userMasterDao){
        return userMasterService.insertUser(userMasterDao);
    }
    @PostMapping ("/updateUser")
    UserMasterDao updateUser(@RequestBody UserMasterDao userMasterDao){
        return userMasterService.updateUser(userMasterDao);
    }

}
