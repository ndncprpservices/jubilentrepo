package com.jubilent.jubilent.CombinePackage.FieldMaster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/FieldMaster")
public class FiledMasterController {

    @Autowired
    FieldMasterService fieldMasterService;

    @PostMapping("/insertField")
    FieldMasterDao insertField(@RequestBody FieldMasterDao fieldMasterDao){

        return fieldMasterService.insertField(fieldMasterDao);
    }
    @GetMapping("/getFieldbyId")
    FieldMasterDao getFieldbyId(@RequestParam(value = "fieldid") long fieldid){

        return fieldMasterService.getFieldbyId(fieldid);
    }
}
