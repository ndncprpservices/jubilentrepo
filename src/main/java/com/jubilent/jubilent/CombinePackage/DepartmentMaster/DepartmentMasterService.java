package com.jubilent.jubilent.CombinePackage.DepartmentMaster;

import java.util.List;

public interface DepartmentMasterService {

    DepartmentMasterDao saveDept (DepartmentMasterDao departmentMasterDao);
    List<DepartmentMasterDao> getActive();
    List<DepartmentMasterDao> getAllDept();
    DepartmentMasterDao updateDept (DepartmentMasterDao departmentMasterDao);
}
