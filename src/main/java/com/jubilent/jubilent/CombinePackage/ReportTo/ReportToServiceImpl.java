package com.jubilent.jubilent.CombinePackage.ReportTo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportToServiceImpl implements ReportToService{

    @Autowired
    ReportToRepo reportToRepo;

    @Override
    public ReportToDao insertReportTO(ReportToDao reportToDao) {
        return reportToRepo.save(reportToDao);
    }

    @Override
    public List<ReportToDao> getAllReportTO() {
        return reportToRepo.findAll();
    }

    @Override
    public List<ReportToDao> getReportTObyUserid(long userid) {
        return reportToRepo.findAllByUserid(userid);
    }

    @Override
    public ReportToDao updateReportTo(ReportToDao reportToDao) {
        ReportToDao reportToDao1 = reportToRepo.findByUserid(reportToDao.getUserid());
        reportToDao1.setReportTo(reportToDao.getReportTo());
        ReportToDao reportToDao2 = reportToRepo.save(reportToDao1);
        return reportToDao2;
    }
}
