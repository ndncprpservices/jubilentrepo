package com.jubilent.jubilent.CombinePackage.ReportTo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "Tbl_ReportTo")
@IdClass(ReportToPrimaryKey.class)
public class ReportToDao  {

    @Id
    private long userid;
    @Id
    private int reportTo;

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public int getReportTo() {
        return reportTo;
    }

    public void setReportTo(int reportTo) {
        this.reportTo = reportTo;
    }

    @Override
    public String toString() {
        return "ReportToDao{" +
                "userid=" + userid +
                ", reportTo=" + reportTo +
                '}';
    }
}
