package com.jubilent.jubilent.CombinePackage.ReportTo;

import java.util.List;

public interface ReportToService {

    ReportToDao insertReportTO(ReportToDao reportToDao);
    List<ReportToDao> getAllReportTO();
    List<ReportToDao> getReportTObyUserid(long userid);
    ReportToDao updateReportTo(ReportToDao reportToDao);

}
