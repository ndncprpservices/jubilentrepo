package com.jubilent.jubilent.CombinePackage.DepartmentMaster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentMasterServiceImpl implements DepartmentMasterService {


    @Autowired
    DepartmentMasterRepo departmentMasterRepo;

    @Override
    public DepartmentMasterDao saveDept(DepartmentMasterDao departmentMasterDao) {
        return departmentMasterRepo.save(departmentMasterDao);
    }

    @Override
    public List<DepartmentMasterDao> getActive() {
        return departmentMasterRepo.getAllByActive(true);
    }

    @Override
    public List<DepartmentMasterDao> getAllDept() {
        return departmentMasterRepo.findAll();
    }

    @Override
    public DepartmentMasterDao updateDept(DepartmentMasterDao departmentMasterDao) {
        DepartmentMasterDao departmentMasterDao1 = departmentMasterRepo.findByDeptid(departmentMasterDao.getDeptid());
        departmentMasterDao1.setActive(departmentMasterDao.isActive());
        departmentMasterDao1.setName(departmentMasterDao.getName());
        departmentMasterDao1.setSequence(departmentMasterDao.getSequence());
        DepartmentMasterDao departmentMasterDao2 = departmentMasterRepo.save(departmentMasterDao1);
        return departmentMasterDao2;
    }


}
