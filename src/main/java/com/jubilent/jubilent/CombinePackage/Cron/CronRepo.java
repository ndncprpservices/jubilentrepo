package com.jubilent.jubilent.CombinePackage.Cron;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CronRepo extends JpaRepository<CronDao,String> {
    CronDao findAllByDateFilter(int dateFilter);
}
