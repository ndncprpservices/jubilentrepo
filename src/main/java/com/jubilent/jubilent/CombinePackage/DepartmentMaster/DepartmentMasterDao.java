package com.jubilent.jubilent.CombinePackage.DepartmentMaster;

import javax.persistence.*;

@Entity
@Table(name = "Tbl_DepartmentMaster")
public class DepartmentMasterDao {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int deptid;
    private String name;
    private boolean active;
    private int sequence;

    public int getDeptid() {
        return deptid;
    }

    public void setDeptid(int deptid) {
        this.deptid = deptid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    @Override
    public String toString() {
        return "DepartmentMasterDao{" +
                "id=" + deptid +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", sequence=" + sequence +
                '}';
    }
}
