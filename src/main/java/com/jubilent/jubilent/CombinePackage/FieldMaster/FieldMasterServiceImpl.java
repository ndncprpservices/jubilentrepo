package com.jubilent.jubilent.CombinePackage.FieldMaster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FieldMasterServiceImpl implements FieldMasterService {

    @Autowired
    FieldMasterRepo fieldMasterRepo;

    @Override
    public FieldMasterDao insertField(FieldMasterDao fieldMasterDao) {
        return fieldMasterRepo.save(fieldMasterDao);
    }

    @Override
    public List<FieldMasterDao> getAllField() {

        return fieldMasterRepo.findAll();
    }

    @Override
    public FieldMasterDao getFieldbyId(long fieldid) {

        System.out.println(fieldid);
        FieldMasterDao fieldMasterDao = fieldMasterRepo.findByFieldId(fieldid);


        return fieldMasterDao;
    }

    @Override
    public FieldMasterDao updateField(FieldMasterDao fieldMasterDao) {

        FieldMasterDao fieldMasterDao1 = fieldMasterRepo.findByFieldId(fieldMasterDao.getFieldId());
        fieldMasterDao1.setFieldName(fieldMasterDao.getFieldName());
        fieldMasterDao1.setPivot(fieldMasterDao.isPivot());
        fieldMasterDao1.setStatus(fieldMasterDao.isStatus());
        FieldMasterDao fieldMasterDao2 = fieldMasterRepo.save(fieldMasterDao1);

        return fieldMasterDao2;
    }
}
