package com.jubilent.jubilent.CombinePackage.UserMaster;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserMasterRepo extends JpaRepository<UserMasterDao,Long> {

    UserMasterDao findByUserid(long userid);
}
