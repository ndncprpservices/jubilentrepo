package com.jubilent.jubilent.CombinePackage.DepartmentMaster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/DepartmentMaster")
public class DepartmentMasterController {


    @Autowired
    DepartmentMasterService departmentMasterService;

    @PostMapping("/saveDept")
    DepartmentMasterDao saveDept(@RequestBody DepartmentMasterDao departmentMasterDao) {

        return departmentMasterService.saveDept(departmentMasterDao);
    }
    @GetMapping("/getAllDept")
    List<DepartmentMasterDao> getAllDept() {
        return departmentMasterService.getAllDept();
    }

}
