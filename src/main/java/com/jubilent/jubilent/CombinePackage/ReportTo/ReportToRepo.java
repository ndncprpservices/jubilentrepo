package com.jubilent.jubilent.CombinePackage.ReportTo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReportToRepo extends JpaRepository<ReportToDao,ReportToPrimaryKey> {

    ReportToDao findByUserid(long userid);
    List<ReportToDao> findAllByUserid(long userid);

}
