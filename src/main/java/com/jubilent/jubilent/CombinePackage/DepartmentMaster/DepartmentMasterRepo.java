package com.jubilent.jubilent.CombinePackage.DepartmentMaster;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartmentMasterRepo extends JpaRepository<DepartmentMasterDao,Integer> {
    List<DepartmentMasterDao> getAllByActive(boolean sts);
    DepartmentMasterDao findByDeptid (long deptid);
}
