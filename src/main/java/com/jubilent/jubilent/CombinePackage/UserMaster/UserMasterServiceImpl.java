package com.jubilent.jubilent.CombinePackage.UserMaster;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserMasterServiceImpl implements  UserMasterService{

    @Autowired
    UserMasterRepo userMasterRepo;

    @Override
    public UserMasterDao insertUser(UserMasterDao userMasterDao) {
        return userMasterRepo.save(userMasterDao);
    }

    @Override
    public List<UserMasterDao> getAllUser() {
        return userMasterRepo.findAll();
    }

    @Override
    public UserMasterDao getbyUserid(long id) {
        return userMasterRepo.findByUserid(id);
    }

    @Override
    public UserMasterDao updateUser(UserMasterDao userMasterDao) {
        UserMasterDao userMasterDao1 = userMasterRepo.findByUserid(userMasterDao.getUserid());
        userMasterDao1.setActive(userMasterDao.isActive());
        userMasterDao1.setEmail(userMasterDao.getEmail());
        userMasterDao1.setUsername(userMasterDao.getUsername());
        userMasterDao1.setPassword(userMasterDao.getPassword());
        userMasterDao1.setPhonenumber(userMasterDao.getPhonenumber());
        userMasterDao1.setDepartmentId(userMasterDao.getDepartmentId());
        userMasterDao1.setName(userMasterDao.getName());
        UserMasterDao userMasterDao2 = userMasterRepo.save(userMasterDao1);
        return userMasterDao2;
    }

}
