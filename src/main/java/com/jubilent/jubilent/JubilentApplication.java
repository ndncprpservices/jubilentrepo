package com.jubilent.jubilent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JubilentApplication {

	public static void main(String[] args) {
		SpringApplication.run(JubilentApplication.class, args);
	}

}
