package com.jubilent.jubilent.CombinePackage.UserMaster;

import com.jubilent.jubilent.CombinePackage.DepartmentMaster.DepartmentMasterDao;

import javax.persistence.*;

@Entity
@Table(name="Tbl_UserMaster" )
public class UserMasterDao {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long userid;
    private String username;
    private String password;
    private String email;
    private String phonenumber;
    private boolean active;
    private String name;
    private long DepartmentId;

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(long departmentId) {
        DepartmentId = departmentId;
    }

    @Override
    public String toString() {
        return "UserMasterDao{" +
                "id=" + userid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                ", active=" + active +
                ", name='" + name + '\'' +
                ", DepartmentId=" + DepartmentId +
                '}';
    }
}
