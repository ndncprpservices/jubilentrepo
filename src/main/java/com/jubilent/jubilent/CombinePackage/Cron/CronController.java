package com.jubilent.jubilent.CombinePackage.Cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Id;
import java.util.List;

@RestController
@RequestMapping("/api/Cron")
public class CronController {


    @Autowired
    CronService cronService;

    @PostMapping("/insertCron")
    CronDao saveCron(@RequestBody CronDao cronDao){
        return cronService.saveCron(cronDao);
    }

    @GetMapping("/getCron")
    CronDao getByDate(@RequestParam("date") String date){
        return cronService.getByDate(date);
    }
    @GetMapping("/getCronall")
    List<CronDao> getByDateall(){
        return cronService.getAll();
    }

}
