package com.jubilent.jubilent.CombinePackage.UserMaster;

import java.util.List;

public interface UserMasterService {
    UserMasterDao insertUser(UserMasterDao userMasterDao);
    UserMasterDao getbyUserid(long id);
    List<UserMasterDao> getAllUser();
    UserMasterDao updateUser(UserMasterDao userMasterDao);

}
