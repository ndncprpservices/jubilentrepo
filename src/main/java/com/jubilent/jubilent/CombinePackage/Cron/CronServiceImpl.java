package com.jubilent.jubilent.CombinePackage.Cron;

import com.jubilent.jubilent.Utility.UtilityClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CronServiceImpl implements CronService{

    @Autowired
    CronRepo cronRepo;


    @Override
    public CronDao saveCron(CronDao cronDao) {
        cronDao.setDateFilter(UtilityClass.convertToDateFilterFromReadble(cronDao.getDate()));
        cronDao.setTime(UtilityClass.getDateMysql());
        return cronRepo.save(cronDao);
    }

    @Override
    public CronDao getByDate(String date) {
        int date2 = UtilityClass.convertReadbleDateToInt(date);
        System.out.println("Date2"+date2);
        CronDao allByDateEquals = cronRepo.findAllByDateFilter(date2);
        return allByDateEquals;
    }

    @Override
    public List<CronDao> getAll() {
        List<CronDao> all = cronRepo.findAll();
        return all;
    }
}
