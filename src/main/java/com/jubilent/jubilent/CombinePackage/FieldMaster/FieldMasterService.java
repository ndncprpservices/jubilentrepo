package com.jubilent.jubilent.CombinePackage.FieldMaster;

import java.util.List;

public interface FieldMasterService {

    FieldMasterDao insertField(FieldMasterDao fieldMasterDao);
    List<FieldMasterDao> getAllField();
    FieldMasterDao getFieldbyId(long fieldid);
    FieldMasterDao updateField(FieldMasterDao fieldMasterDao);
}
